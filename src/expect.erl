-module(expect).

-export([spawn/1]).
-export([send/1]).
-export([expect/1]).

-type msg() :: term().
-type path() :: term().

-spec spawn(path()) -> ok.
-spec send(msg()) -> ok.
-spec expect(msg()) -> ok.
